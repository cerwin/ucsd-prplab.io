## README

This site is based on Example [Jekyll] website using GitLab Pages.  
See the original
[README](https://gitlab.com/ucsd-prp/ucsd-prp.gitlab.io/tree/master/howto/README-jekyll.md)
to get started with Jekyll 

### howto/

Files in [howto](https://gitlab.com/ucsd-prp/ucsd-prp.gitlab.io/tree/master/howto )
are helper scripts to download and convert original Plone website. 

* download-website - download the original website given its URL

